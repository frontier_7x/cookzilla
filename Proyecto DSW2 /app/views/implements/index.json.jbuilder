json.array!(@implements) do |implement|
  json.extract! implement, :id, :nombre
  json.url implement_url(implement, format: :json)
end
