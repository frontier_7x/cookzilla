json.array!(@ingredients) do |ingredient|
  json.extract! ingredient, :id, :nombre
  json.url ingredient_url(ingredient, format: :json)
end
