class SearchRecipeController < ApplicationController
  def index
	@resultado=Recipe.joins(:ingredients, :implements).where(ingredients: {id: params[:ingredients]}, implements: {id: params[:implements]})
	@resultado=@resultado.uniq
  @resultado=@resultado.where(state: 'published')

    #raise @resultado.to_yaml
  end

  def search_form
      @ingredients = Ingredient.all
      @implements = Implement.all
    
  end
end