class RecipesController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :authenticate_normal_user!, only: [:create, :new]
  before_action :authenticate_moderador!, only: [:publish]
  before_action :authenticate_admin!, only: [:destroy, :edit, :update]
  before_action :set_recipe, only: [:show, :edit, :update, :destroy, :publish]

  # GET /recipes
  # GET /recipes.json
  def index
    if user_signed_in? && current_user.is_admin? 
      @recipes = Recipe.all
    elsif user_signed_in? && current_user.is_normal_user? 
      @recipes = current_user.recipes.all
    elsif user_signed_in? && current_user.is_moderador?
      @recipes = Recipe.all 
      #@recipes = Recipe.where(:ingredientes => ingredientes)
    else
    end
  end

  # GET /recipes/1
  # GET /recipes/1.json
  def show
    @comment = Comment.new
  end

  # GET /recipes/new
  def new
    @recipe = Recipe.new
    @ingredients = Ingredient.aceptados
    @implements = Implement.aceptados
    
  end

  def publish
    @recipe.publish!
    redirect_to @recipe
  end


  # GET /recipes/1/edit
  def edit
    @ingredients = Ingredient.all
    @implements = Implement.all
  end

  # POST /recipes
  # POST /recipes.json
  def create
    @recipe = current_user.recipes.new(recipe_params)
    #Asignas los valores de implementos seleccionados a la variable implementos
    @recipe.implements = params[:Implements]
    @recipe.ingredients = params[:Ingredients]
    if params[:Implements].nil? or params[:Ingredients].nil?
      respond_to do |format|
          format.html { redirect_to '/recipes/new', alert: 'Ingrese entre 1 a 10 ingredientes y entre 1 a 5 implementos' }
          format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    elsif params[:Implements].size > 5 or params[:Ingredients].size > 10
      respond_to do |format|
          format.html { redirect_to '/recipes/new', alert: 'Ingrese entre 1 a 10 ingredientes y entre 1 a 5 implementos' }
          format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    
    #Asignas los valores de ingredientes seleccionados a la variable ingredientes
    #@recipe.ingredients = params[:Ingredients]
    #raise params.yaml
      elsif user_signed_in? && current_user.is_normal_user? 
        respond_to do |format|
          if @recipe.save
            format.html { redirect_to @recipe, notice: 'La receta fue creada con exito.' }
            format.json { render :show, status: :created, location: @recipe }
          else
            format.html { render :new }
            format.json { render json: @recipe.errors, status: :unprocessable_entity }
          end 
        end
      else
      respond_to do |format|  
        format.html { redirect_to @recipe, notice: 'No puedes crear Recetas' }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recipes/1
  # PATCH/PUT /recipes/1.json
  def update
    if user_signed_in? && current_user.is_admin? 
      respond_to do |format|
        if @recipe.update(recipe_params)
          format.html { redirect_to @recipe, notice: 'Se ha modificado la receta con exito.' }
          format.json { render :show, status: :ok, location: @recipe }
        else
          format.html { render :edit }
          format.json { render json: @recipe.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to recipes_url, notice: 'No tiene permisos para modificar recetas.' }
        format.json { head :no_content }
      end
    end
  end
  def validate_user
    redirect_to new_user_session_path, notice:"Se necesita iniciar sesion"
  end

  # DELETE /recipes/1
  # DELETE /recipes/1.json
  def destroy
    if user_signed_in? && current_user.is_admin? 
      @recipe.destroy
      respond_to do |format|
        format.html { redirect_to recipes_url, notice: 'Se ha eliminado la receta de forma exitosa.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to recipes_url, notice: 'No se tienen los permisos para eliminar recetas.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recipe_params
      params.require(:recipe).permit(:titulo, :dificultad, :tiempo, :Ingredients, :Implements, :glosa)
    end
end
