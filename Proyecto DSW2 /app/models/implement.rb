class Implement < ActiveRecord::Base
	include AASM
	has_many :recipe_implements
	has_many :recipes, through: :recipe_implementss


    scope :aceptados, ->{where(state: "published")}
    aasm column: "state" do
        state :in_draft, initial: true
        state :published

        event :publish do
            transitions from: :in_draft, to: :published
        end

        event :unpublish do
            transitions from: :published, to: :in_draft
        end
    end
end
