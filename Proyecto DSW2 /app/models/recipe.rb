class Recipe < ActiveRecord::Base
    include AASM
    after_create :save_implements
    after_create :save_ingredients
    has_many :recipe_implements, dependent: :destroy
    has_many :recipe_ingredients, dependent: :destroy
    has_many :ingredients, through: :recipe_ingredients
    has_many :implements, through: :recipe_implements
    accepts_nested_attributes_for :ingredients
    accepts_nested_attributes_for :implements 
    has_many :comments   

    belongs_to :user

    scope :aceptadas, ->{where(state: "published")}
    
        #CUSTOM SETTER#
	def ingredients=(value)
            @ingredients = value
	end
	def implements=(value)
            @implements = value
            #raise @implementos.to_yaml
	end
        
        
    aasm column: "state" do
        state :in_draft, initial: true
        state :published

        event :publish do
            transitions from: :in_draft, to: :published
        end

        event :unpublish do
            transitions from: :published, to: :in_draft
        end
    end
    private
        
    #Guarda los implemenos de una receta
    def save_implements
        #raise self.id.to_yaml
        @implements.each do |implement_id|
            recipe_implements.create(implement_id: implement_id, recipe_id: self.id) 
        end
    end
        
    def save_ingredients
        #raise @ingredientes.to_yaml
        @ingredients.each do |ingredient_id|
            recipe_ingredients.create(ingredient_id: ingredient_id, recipe_id: self.id)
        end
                
    end


end