class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :titulo
      t.string :dificultad
      t.string :tiempo
      t.string :ingredientes
      t.string :implemento
      t.string :glosa

      t.timestamps null: false
    end
  end
end
