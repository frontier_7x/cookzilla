class ChangeColumnTime < ActiveRecord::Migration
  def change
  	remove_column :recipes, :tiempo, :string
  	add_column :recipes, :tiempo, :datetime
  end
end
