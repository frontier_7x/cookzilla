class AddImplement < ActiveRecord::Migration
  def change
  	add_column :implements, :state, :string, default: "in_draft"
  end
end
