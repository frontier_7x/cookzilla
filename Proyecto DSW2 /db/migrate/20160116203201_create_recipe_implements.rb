class CreateRecipeImplements < ActiveRecord::Migration
  def change
    create_table :recipe_implements do |t|
      t.belongs_to :implement, index: true
      t.belongs_to :recipe, index: true
      t.timestamps null: false
    end
  end
end
