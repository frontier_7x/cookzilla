class AddColumnRecipe < ActiveRecord::Migration
  def change
  	add_column :recipes, :state, :string, default: "in_draft"
  end
end
