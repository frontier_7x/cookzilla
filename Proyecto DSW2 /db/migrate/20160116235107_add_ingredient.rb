class AddIngredient < ActiveRecord::Migration
  def change
  	add_column :ingredients, :state, :string, default: "in_draft"
  end
end
