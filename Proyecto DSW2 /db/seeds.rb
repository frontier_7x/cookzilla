# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
#
ingredients = Ingredient.create([{ nombre: 'Soya', state: 'published' }, { nombre: 'Pimienta', state: 'published' }, { nombre: 'Queso rallado', state: 'published' }, { nombre: 'Harina', state: 'published' }, { nombre: 'Laurel', state: 'published' }, { nombre: 'Papas', state: 'published' }, { nombre: 'Pollo', state: 'published' }, { nombre: 'Arroz', state: 'published' }])
implements = Implement.create([{ nombre: 'Parrilla a Gas', state: 'published' }, { nombre: 'Horno', state: 'published' }])
