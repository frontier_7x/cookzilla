# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160119195833) do

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id",    limit: 38,  precision: 38
    t.integer  "recipe_id",  limit: 38,  precision: 38
    t.text     "body",       limit: nil
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "comments", ["recipe_id"], name: "index_comments_on_recipe_id"
  add_index "comments", ["user_id"], name: "index_comments_on_user_id"

  
  create_table "implements", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "state",      default: "in_draft"
  end

  create_table "ingredients", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "state",      default: "in_draft"
  end

  create_table "recipe_implements", force: :cascade do |t|
    t.integer  "implement_id", limit: 38, precision: 38
    t.integer  "recipe_id",    limit: 38, precision: 38
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "recipe_implements", ["implement_id"], name: "i_rec_imp_imp_id"
  add_index "recipe_implements", ["recipe_id"], name: "i_recipe_implements_recipe_id"

  create_table "recipe_ingredients", force: :cascade do |t|
    t.integer  "ingredient_id", limit: 38, precision: 38
    t.integer  "recipe_id",     limit: 38, precision: 38
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "recipe_ingredients", ["ingredient_id"], name: "i_rec_ing_ing_id"
  add_index "recipe_ingredients", ["recipe_id"], name: "i_recipe_ingredients_recipe_id"

  create_table "recipes", force: :cascade do |t|
    t.string   "titulo"
    t.string   "dificultad"
    t.string   "ingredientes"
    t.string   "implemento"
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.integer  "user_id",      limit: 38,   precision: 38
    t.string   "state",                                    default: "in_draft"
    t.string   "glosa",        limit: 4000
    t.datetime "tiempo"
  end

  add_index "recipes", ["user_id"], name: "index_recipes_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                                            default: "", null: false
    t.string   "encrypted_password",                               default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 38, precision: 38, default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "name"
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.integer  "permission_level",       limit: 38, precision: 38, default: 1
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "i_users_reset_password_token", unique: true

  add_foreign_key "comments", "recipes"
  add_foreign_key "comments", "users"
  add_foreign_key "recipes", "users"
end
